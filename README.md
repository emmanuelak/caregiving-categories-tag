# discourse-categories-tag-column

Forked from: https://github.com/discourse/discourse-categories-tag-column

Adds a configurable tags column to the "categories and top/latest" category page styles.
